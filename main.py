#d1->gpio5
#d0->gpio16
#scl->d2->gpio4
#sda->d3->gpio0

import dht
from machine import Pin, I2C
from time import sleep
import ssd1306



dht22_ = dht.DHT22(Pin(5))
rellay_port = Pin(16, Pin.OUT)
i2c = I2C(scl=Pin(4), sda=Pin(0))
oled_width = 128
oled_height = 64
oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)
status_rellay = True
text_suhu = ''
text_hum = ''
text_suhu_oled = ''
text_hum_oled = ''


def rellay(value):
  rellay_port(value)
 
def oled_(data1, data2):
  oled.fill(0)
  oled.show()
  oled.text(text_suhu,20, 20)
  oled.text(text_hum, 5, 40)    
  oled.show()
  
   
    

def dht22():
  try:
   dht22_.measure()
   temp = dht22_.temperature()
   hum = dht22_.humidity()
   return{'msg':'ok', 'temp':temp, 'hum':hum}
  except OSError as e:
    print(e)
    return {'msg':'sensor error', 'temp':0, 'hum':0}

oled.text('Didi Elektro...',0, 35)
oled.show()
sleep(2)
while True:
  suhu = dht22()
  print(suhu['temp'])
  print(suhu['hum'])
  text_suhu ='Suhu: '+str(int(suhu['temp'])) + ' C'
  text_hum = 'Kelembaban: '+str(int(suhu['hum'])) + '%'
  if text_suhu != text_suhu_oled or text_hum != text_hum_oled:
    text_suhu_oled = text_suhu
    text_hum_oled = text_hum
    oled_(text_suhu, text_hum)
  
  if suhu['temp'] <=37.0 and status_rellay==True:
    status_rellay = False
    rellay(status_rellay)
  elif suhu['temp'] >= 38.7 and  status_rellay==False:
    status_rellay = True
    rellay(status_rellay)
  









